<?php
/*
* Securité : Empêche d'accéder au fichier via une url
*/
if ( !defined( 'ABSPATH' ) ) {
	exit; // Le fichier ne peut être exécuté que dans l'environnement WP (afin d'éviter les attaques)
}

class BeApi_Counter {
	/**
	 * Undocumented function
	 *
	 * @param [type] $post_id
	 * @param [type] $post
	 * @param [type] $update
	 * @return void
	 */
	public static function setCounter( $new_status, $old_status, $post ) {
		if ( 'publish' === $new_status && 'publish' !== $old_status ) {
			$views = get_post_meta( $post->ID, 'post_views', true );
			$views = ( $views ) ? $views + 1 : 0;

			update_post_meta( $post->ID, 'post_views', $views );
		}
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $columns
	 * @return void
	 */
	public static function beapi_addCounterColumn( $columns ) {
		$columns['post_views'] = 'Views';
		return $columns;
	}

	/**
	 * Undocumented function
	 *
	 * @param [type] $column
	 * @param [type] $post_id
	 * @return void
	 */
	public static function beapi_displayCounterColumn( $column, $post_id ) {
		if ( 'post_views' === $column ) {
			$post_views = get_post_meta( $post_id, 'post_views', true );
			echo esc_html( $post_views );
		}
	}
}

// Hooker la méthode setTinyUrl à l'action transition_post_status
add_action( 'transition_post_status', array('BeApi_Counter', 'setCounter'), 10, 3 );

// Ajouter la colonne custom dans l'edit panel
add_filter( 'manage_post_posts_columns', array( 'BeApi_Counter', 'beapi_addCounterColumn' ) );
add_filter( 'manage_post_pages_columns', array( 'BeApi_Counter', 'beapi_addCounterColumn' ) );

// Afficher la meta Tiny URL dans la colonne custom
add_action( 'manage_post_posts_custom_column', array( 'BeApi_Counter', 'beapi_displayCounterColumn' ), 10, 2 );
add_action( 'manage_post_pages_custom_column', array( 'BeApi_Counter', 'beapi_displayCounterColumn' ), 10, 2 );
