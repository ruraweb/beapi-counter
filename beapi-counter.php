<?php
/*
Plugin Name: Be Api - Counter
Plugin URI:
Description: Counter of posts
Author: Be Api
Author URI:
Version: 1.0
*/

/*
* Securité : Empêche d'accéder au fichier via une url
*/
if ( !defined( 'ABSPATH' ) ) {
	exit; // Le fichier ne peut être exécuté que dans l'environnement WP (afin d'éviter les attaques)
}

/*
* Constantes du plugin
*/
if ( ! defined( 'BEAPI_COUNTER_DIR' ) ) {
	define( 'BEAPI_COUNTER_DIR', plugin_dir_path( __FILE__ ) ); // PATH du dossier du plugin
}

/*
* Fichiers inclus
*/
require_once BEAPI_COUNTER_DIR . 'src/beapi_counter.php';

/**
 * Undocumented function
 *
 * @return void
 */
function beapi_output_increment_post_views() {
	if ( is_singular() || is_page() ) {
		$post_id = get_the_ID();
		$views   = get_post_meta( $post_id, 'post_views', true );
		$views   = ( $views ) ? $views + 1 : 1;

		update_post_meta( $post_id, 'post_views', $views );
	}
}
add_action( 'wp_head', 'beapi_output_increment_post_views' );
