# Counter of posts

## Usage

Il est demandé de créer un plugin de compteurs de vues dans WordPress, où un plugin de cache statique est installé (WP-Super-Cache par exemple). Il faut pouvoir compter les vues pour chaque article (page, post, cpt).  

Il faut pouvoir afficher les contenus les plus populaires à travers une simple WP_Query.

### QUERY
```php
$args = array(
	'post_type'      => 'post',
	'posts_per_page' => 5,
	'meta_key'       => 'post_views',
	'orderby'        => 'meta_value_num',
	'order'          => 'DESC',
);

$popular_posts = new WP_Query( $args );

if ($popular_posts->have_posts()) {
	while ($popular_posts->have_posts()) {
		$popular_posts->the_post();
		?>
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<p>Nombre de vues : <?php echo get_post_meta(get_the_ID(), 'post_views', true); ?></p>
		<?php
	}
	wp_reset_postdata();
} else {
	echo 'Aucun post trouvé.';
}
```
